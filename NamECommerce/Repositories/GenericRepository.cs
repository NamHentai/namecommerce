﻿using Microsoft.EntityFrameworkCore;
using NamECommerce.Entities;
using NamECommerce.Repositories.IRepositories;
using OnlineBookstore.API;
using System.Linq.Expressions;
using NamECommerce.Entities.Enums;

namespace NamECommerce.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : Base
    {

        protected readonly ApplicationDbContext _applicationDbContext;
        protected readonly DbSet<T> _db;
        public GenericRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
            _db = _applicationDbContext.Set<T>();
        }

        public async Task CreateAsync(T entity)
        {
            await _db.AddAsync(entity);
        }

        public async Task<bool> ExistAsync(Guid id)
        {
            return await _db.AnyAsync(x => x.Id == id);

        }

        public async Task<List<T>> GetAllAdvanceAsync<TKey>(Expression<Func<T, bool>> filter = null,
            int pageSize = 10,
            int pageIndex = 1,
            Expression<Func<T, TKey>> keySelectorForSort = null,
            Sort sort = Sort.Ascending)
        {
            var query = _db.AsQueryable();
            if (filter != null)
            {
                query = query.Where(filter);
            }
            query.OrderBy(keySelectorForSort);
            if (sort == Sort.Descending)
            {
                query.OrderByDescending(keySelectorForSort);
            }
            if (pageSize > 0 && pageIndex > 0)
            {
                return await query
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();
            }
            return await query.ToListAsync();
        }

        public async Task<List<T>> GetAllAsync(Expression<Func<T, bool>> filter = null)
        {
            IQueryable<T> queryable = _db;
            if (filter != null)
            {
                queryable = queryable.Where(filter);
            }
            return await queryable.ToListAsync();
        }

        public async Task<T> GetAsync(Expression<Func<T, bool>>? filter = null)
        {
            _db.AsNoTracking();
            if (filter != null)
            {
                return await _db.FirstOrDefaultAsync(filter);
            }
            return await _db.FirstOrDefaultAsync();
        }

        public async Task<bool> IsDeletedAsync(Guid id)
        {
            var customer = await _db.FirstOrDefaultAsync(x => x.Id == id);
            return customer.IsDelete;
        }

        public async Task RemoveAsync(T entity)
        {
            _db.Remove(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            _db.Update(entity);
        }
    }
}
