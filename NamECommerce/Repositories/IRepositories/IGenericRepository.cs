﻿using NamECommerce.Entities;
using NamECommerce.Entities.Enums;
using System.Linq.Expressions;

namespace NamECommerce.Repositories.IRepositories
{
    public interface IGenericRepository<T> where T : Base
    {
        Task<T> GetAsync(Expression<Func<T, bool>>? filter = null);
        Task<List<T>> GetAllAsync(Expression<Func<T, bool>>? filter = null);
        Task CreateAsync(T entity);
        Task UpdateAsync(T entity);
        Task RemoveAsync(T entity);
        Task<bool> ExistAsync(Guid id);
        Task<bool> IsDeletedAsync(Guid id);
        Task<List<T>> GetAllAdvanceAsync<TKey>(
            Expression<Func<T, bool>> filter = null,
            int pageSize = 0,
            int pageIndex = 1,
            Expression<Func<T, TKey>> keySelectorForSort = null,
            Sort sortType = Sort.Ascending);
    }
}
