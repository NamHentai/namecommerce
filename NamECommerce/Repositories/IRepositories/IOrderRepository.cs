﻿using NamECommerce.Entities;

namespace NamECommerce.Repositories.IRepositories
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
    }
}
