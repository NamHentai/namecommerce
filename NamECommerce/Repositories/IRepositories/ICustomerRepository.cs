﻿using NamECommerce.Entities;
using NamECommerce.Entities.Dtos;
using System.Linq.Expressions;

namespace NamECommerce.Repositories.IRepositories
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        Task<Customer> GetAllOrdersAsync(Expression<Func<Customer, bool>> filter = null);
    }
}
