﻿namespace NamECommerce.Repositories.IRepositories
{
    public interface IUnitOfWork
    {
        ICustomerRepository Customers { get; }
        IOrderRepository Orders { get; }
        IOrderItemRepository OrderItems { get; }
        IProductRepository Products { get; }
        Task SaveAsync();
    }
}
