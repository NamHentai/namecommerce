﻿using NamECommerce.Entities;

namespace NamECommerce.Repositories.IRepositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
    }
}
