﻿using NamECommerce.Entities;

namespace NamECommerce.Repositories.IRepositories
{
    public interface IOrderItemRepository : IGenericRepository<OrderItem>
    {
        public Task<List<OrderItem>> GetByOrderAsync(Guid id);
    }
}
