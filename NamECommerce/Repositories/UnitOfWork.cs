﻿using NamECommerce.Repositories.IRepositories;
using OnlineBookstore.API;

namespace NamECommerce.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        ApplicationDbContext _context;
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            Customers = new CustomerRepository(context);
            Orders = new OrderRepository(context);
            OrderItems = new OrderItemRepository(context);
            Products = new ProductRepository(context);
        }

        public ICustomerRepository Customers { get; }

        public IOrderRepository Orders { get; }

        public IOrderItemRepository OrderItems { get; }

        public IProductRepository Products { get; }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
