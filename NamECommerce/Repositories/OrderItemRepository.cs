﻿using Microsoft.EntityFrameworkCore;
using NamECommerce.Entities;
using NamECommerce.Repositories.IRepositories;
using OnlineBookstore.API;

namespace NamECommerce.Repositories
{
    public class OrderItemRepository : GenericRepository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }

        public async Task<List<OrderItem>> GetByOrderAsync(Guid id)
        {
            return await _db.Where(x => x.OrderId == id).ToListAsync();
        }
    }
}
