﻿using Microsoft.EntityFrameworkCore;
using NamECommerce.Entities;
using NamECommerce.Entities.Dtos;
using NamECommerce.Repositories.IRepositories;
using OnlineBookstore.API;
using System;
using System.Linq.Expressions;

namespace NamECommerce.Repositories
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }

        public async Task<Customer> GetAllOrdersAsync(Expression<Func<Customer, bool>> filter = null)
        {
            IQueryable<Customer> query = _db;
            var result = await query.Include(x => x.Orders).FirstOrDefaultAsync(filter);
            return result;
        }
    }
}
