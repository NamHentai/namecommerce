﻿using Microsoft.EntityFrameworkCore;
using NamECommerce.Entities;
using NamECommerce.Repositories.IRepositories;
using OnlineBookstore.API;

namespace NamECommerce.Repositories
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }

        public async Task<List<Order>> GetByCustomerAsync(Guid id)
        {
            return await _db.Where(x => x.CustomerId == id).ToListAsync();
        }
    }
}
