﻿using NamECommerce.Entities;
using NamECommerce.Repositories.IRepositories;
using OnlineBookstore.API;

namespace NamECommerce.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }
    }
}
