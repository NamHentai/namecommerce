﻿using AutoMapper;
using NamECommerce.Entities;
using NamECommerce.Entities.Dtos;
using NamECommerce.Entities.Enums;
using NamECommerce.Repositories.IRepositories;
using NamECommerce.Services.IServices;
using System.Linq.Expressions;

namespace NamECommerce.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ServiceResponse _apiResponse;

        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _apiResponse = new ServiceResponse();
        }

        public async Task<ServiceResponse> AddOrderItem(OrderItemDto orderItemDto)
        {
            var orderItem = _mapper.Map<OrderItem>(orderItemDto);
            await _unitOfWork.OrderItems.CreateAsync(orderItem);
            await _unitOfWork.SaveAsync();
            return _apiResponse.Response();
        }

        public async Task<ServiceResponse> Create(OrderCreateDto orderCreateDto)
        {
            var order = _mapper.Map<Order>(orderCreateDto);
            await _unitOfWork.Orders.CreateAsync(order);
            await _unitOfWork.SaveAsync();
            return _apiResponse.Response();
        }

        public async Task<ServiceResponse> DeleteById(Guid id)
        {
            await _unitOfWork.Orders.GetAsync(x => x.Id == id);
            await _unitOfWork.SaveAsync();
            return _apiResponse.Response();
        }

        public async Task<ServiceResponse> DeleteOrderItem(Guid id)
        {
            await _unitOfWork.OrderItems.GetAsync(x => x.Id == id);
            await _unitOfWork.SaveAsync();
            return _apiResponse.Response();
        }

        public async Task<ServiceResponse> GetAll()
        {
            var result = await _unitOfWork.Orders.GetAllAsync();
            return _apiResponse.Response(result);
        }

        public async Task<ServiceResponse> GetAllAdvanceAsync(Expression<Func<Order, bool>> filter = null,
            int pageSize = 0,
            int pageIndex = 1,
            Sort sort = Sort.Ascending)
        {
            var result = await _unitOfWork.Orders.GetAllAdvanceAsync<string>(
                filter,
                pageSize,
                pageIndex,
                keySelectorForSort: x => x.Id.ToString(),
                sort
                );
            return _apiResponse.Response(result);
        }

        public async Task<ServiceResponse> GetById(Guid id)
        {
            var result = await _unitOfWork.Orders.GetAsync(x => x.Id == id);
            return _apiResponse.Response(result);
        }

        public async Task<ServiceResponse> GetOrderItems(Guid id)
        {
            var result1 = await _unitOfWork.Orders.GetAsync(x => x.Id == id);
            return _apiResponse.Response(result1);
        }

        public async Task<ServiceResponse> SoftDeleteById(Guid id)
        {
            var result = await _unitOfWork.Orders.GetAsync(x => x.Id == id);
            return _apiResponse.Response(result);
        }

        public async Task<ServiceResponse> UpdateOrderItem(OrderItemDto orderItemDto)
        {
            var orderItem = _mapper.Map<OrderItem>(orderItemDto);
            orderItem.ModifiedDate = DateTime.Now;
            await _unitOfWork.OrderItems.UpdateAsync(orderItem);
            await _unitOfWork.SaveAsync();
            return _apiResponse.Response(orderItem);
        }
    }
}
