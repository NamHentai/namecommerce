﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NamECommerce.Entities;
using NamECommerce.Entities.Dtos;
using NamECommerce.Entities.Enums;
using NamECommerce.Repositories.IRepositories;
using NamECommerce.Services.IServices;
using System.Linq.Expressions;

namespace NamECommerce.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ServiceResponse _apiResponse;

        public CustomerService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _apiResponse = new ServiceResponse();
        }

        public async Task<ServiceResponse> Create(CustomerCreateDto dto)
        {
            Customer customer = _mapper.Map<Customer>(dto);
            await _unitOfWork.Customers.CreateAsync(customer);
            await _unitOfWork.SaveAsync();
            return _apiResponse.Response(dto);
        }

        public async Task<ServiceResponse> DeleteById(Guid id)
        {
            var customerDto = await _unitOfWork.Customers.GetAsync(x => x.Id == id);
            Customer customer = _mapper.Map<Customer>(customerDto);
            await _unitOfWork.Customers.RemoveAsync(customer);
            await _unitOfWork.SaveAsync();
            return _apiResponse.Response("Thanh cong");
        }

        public async Task<ServiceResponse> GetAll()
        {
            var customers = await _unitOfWork.Customers.GetAllAsync();
            List<CustomerDto> customerDtos = _mapper.Map<List<CustomerDto>>(customers);
            return _apiResponse.Response(customerDtos);
        }

        public async Task<ServiceResponse> GetById(Guid id)
        {
            var check = await _unitOfWork.Customers.IsDeletedAsync(id);
            if (check)
            {
                return _apiResponse.Response("Khong co customerId nay!");
            }
            var customer = await _unitOfWork.Customers.GetAsync(x => x.Id == id);
            var customerDto = _mapper.Map<CustomerDto>(customer);
            return _apiResponse.Response(customerDto);
        }

        public async Task<ServiceResponse> GetOrders(Guid id)
        {
            var customer = await _unitOfWork.Customers.GetAllOrdersAsync();
            return _apiResponse.Response(customer);

        }

        public async Task<ServiceResponse> SoftDeleteById(Guid id)
        {
            var customer = await _unitOfWork.Customers.GetAsync(x => x.Id == id);
            customer.IsDelete = true;
            await _unitOfWork.SaveAsync();
            return _apiResponse.Response("thanh cong");
        }

        public async Task<ServiceResponse> Update(CustomerDto customerDto)
        {
            var customer = _mapper.Map<Customer>(customerDto);
            customer.ModifiedDate = DateTime.Now;
            await _unitOfWork.Customers.UpdateAsync(customer);
            await _unitOfWork.SaveAsync();
            return _apiResponse.Response(customer);
        }

        public async Task<ServiceResponse> GetAllAdvanceAsync(Expression<Func<Customer, bool>> filter = null,
            int pageSize = 0,
            int pageIndex = 1,
            Sort sort = Sort.Ascending)
        {
            var result = await _unitOfWork.Customers.GetAllAdvanceAsync<string>(
                filter,
                pageSize,
                pageIndex,
                keySelectorForSort: x => x.Name,
                sort
                );
            return _apiResponse.Response(result);
        }

        public Task<ServiceResponse> GetAllOrdersOfCustomerAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<ServiceResponse> GetTotalOrdersOfCustomerAsync(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
