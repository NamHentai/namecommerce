﻿using NamECommerce.Entities;
using NamECommerce.Entities.Dtos;
using NamECommerce.Entities.Enums;
using System.Linq.Expressions;

namespace NamECommerce.Services.IServices
{
    public interface ICustomerService
    {
        Task<ServiceResponse> Create(CustomerCreateDto customer);
        Task<ServiceResponse> GetAll();
        Task<ServiceResponse> GetById(Guid id);
        Task<ServiceResponse> Update(CustomerDto customerDto);
        Task<ServiceResponse> DeleteById(Guid id);
        Task<ServiceResponse> SoftDeleteById(Guid id);
        Task<ServiceResponse> GetOrders(Guid id);
        Task<ServiceResponse> GetAllAdvanceAsync(
            Expression<Func<Customer, bool>> filter = null,
            int pageSize = 0,
            int pageIndex = 1,
            Sort sort = Sort.Ascending);
        Task<ServiceResponse> GetAllOrdersOfCustomerAsync(Guid id);
        Task<ServiceResponse> GetTotalOrdersOfCustomerAsync(Guid id);

    }
}
