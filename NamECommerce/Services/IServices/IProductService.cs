﻿using NamECommerce.Entities;
using NamECommerce.Entities.Dtos;

namespace NamECommerce.Services.IServices
{
    public interface IProductService
    {
        Task<ServiceResponse> Create(ProductCreateDto productCreateDto);
        Task<ServiceResponse> GetAll();
        Task<ServiceResponse> GetById(Guid id);
        Task<ServiceResponse> Update(ProductDto productDto);
        Task<ServiceResponse> DeleteById(Guid id);
        Task<ServiceResponse> SoftDeleteById(Guid id);
    }
}
