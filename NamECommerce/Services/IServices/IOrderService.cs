﻿using NamECommerce.Entities.Dtos;
using NamECommerce.Entities.Enums;
using NamECommerce.Entities;
using System.Linq.Expressions;

namespace NamECommerce.Services.IServices
{
    public interface IOrderService
    {
        Task<ServiceResponse> Create(OrderCreateDto orderCreateDto);
        Task<ServiceResponse> GetAll();
        Task<ServiceResponse> GetById(Guid id);
        Task<ServiceResponse> DeleteById(Guid id);
        Task<ServiceResponse> SoftDeleteById(Guid id);
        Task<ServiceResponse> GetOrderItems(Guid id);
        Task<ServiceResponse> AddOrderItem(OrderItemDto orderItemDto);
        Task<ServiceResponse> DeleteOrderItem(Guid id);
        Task<ServiceResponse> UpdateOrderItem(OrderItemDto orderItemDto);
        Task<ServiceResponse> GetAllAdvanceAsync(
            Expression<Func<Order, bool>> filter = null,
            int pageSize = 0,
            int pageIndex = 1,
            Sort sort = Sort.Ascending);
    }
}
