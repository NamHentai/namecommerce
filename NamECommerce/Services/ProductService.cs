﻿using AutoMapper;
using NamECommerce.Entities;
using NamECommerce.Entities.Dtos;
using NamECommerce.Repositories.IRepositories;
using NamECommerce.Services.IServices;

namespace NamECommerce.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ServiceResponse _apiResponse;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ServiceResponse> Create(ProductCreateDto productCreateDto)
        {
            var product = _mapper.Map<Product>(productCreateDto);
            await _unitOfWork.Products.CreateAsync(product);
            await _unitOfWork.SaveAsync();
            return _apiResponse.Response(product);
        }

        public async Task<ServiceResponse> DeleteById(Guid id)
        {
            var productDto = await _unitOfWork.Products.GetAsync(x => x.Id == id);
            var product = _mapper.Map<Product>(productDto);
            await _unitOfWork.Products.RemoveAsync(product);
            await _unitOfWork.SaveAsync();
            return _apiResponse.Response();
        }

        public async Task<ServiceResponse> GetAll()
        {
            var products = await _unitOfWork.Products.GetAllAsync();
            List<ProductDto> productDtos = _mapper.Map<List<ProductDto>>(products);
            return _apiResponse.Response(products);
        }

        public Task<ServiceResponse> GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<ServiceResponse> SoftDeleteById(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<ServiceResponse> Update(ProductDto productDto)
        {
            throw new NotImplementedException();
        }
    }
}
