﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using NamECommerce.Entities;

namespace NamECommerce.FluentApi
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(e => e.Total)
            .HasPrecision(18, 2);
            builder.HasMany(x=>x.OrderItems).WithOne(x => x.Order).HasForeignKey(x => x.Id);
        }
    }
}
