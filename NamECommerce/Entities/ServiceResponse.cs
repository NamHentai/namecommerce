﻿namespace NamECommerce.Entities
{
    public class ServiceResponse
    {
        private string _message { get; set; }
        public object _result { get; set; }

        public ServiceResponse Response(object result = null, string message = null)
        {
            _result = result;
            _message = message;
            return this;
        }
    }
}
