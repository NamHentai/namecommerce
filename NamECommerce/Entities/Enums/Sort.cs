﻿namespace NamECommerce.Entities.Enums
{
    public enum Sort
    {
        Ascending,
        Descending
    }
}
