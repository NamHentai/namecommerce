﻿namespace NamECommerce.Entities
{
    public class Base
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime? ModifiedDate { get; set; }
        public bool IsDelete { get; set; } = false;
    }
}
