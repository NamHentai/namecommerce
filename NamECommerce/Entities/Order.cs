﻿namespace NamECommerce.Entities
{
    public class Order : Base
    {
        public decimal Total { get; set; }
        public Guid CustomerId { get; set; }
        public Customer Customer { get; set; }
        public ICollection<OrderItem> OrderItems { get; set; }
    }
}
