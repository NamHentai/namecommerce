﻿namespace NamECommerce.Entities.Dtos
{
    public class OrderCreateDto
    {
        public Guid CustomerId { get; set; }
        public ICollection<OrderItemCreateDto> OrderItems { get; set; }
    }
}
