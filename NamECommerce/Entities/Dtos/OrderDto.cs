﻿namespace NamECommerce.Entities.Dtos
{
    public class OrderDto
    {
        public ICollection<OrderItemCreateDto> OrderItems { get; set; }
    }
}
