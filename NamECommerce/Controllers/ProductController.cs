﻿using Microsoft.AspNetCore.Mvc;
using NamECommerce.Entities.Dtos;
using NamECommerce.Services;
using NamECommerce.Services.IServices;

namespace NamECommerce.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var serviceResponse = await _productService.GetAll();
            return Ok(serviceResponse._result);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var serviceResponse = await _productService.GetById(id);
            return Ok(serviceResponse._result);
        }
        [HttpPost]
        public async Task<IActionResult> Post(ProductCreateDto productCreateDto)
        {
            var serviceResponse = await _productService.Create(productCreateDto);
            return Ok(serviceResponse._result);
        }
        [HttpPut]
        public async Task<IActionResult> Put(OrderDto orderDto)
        {
            return Ok();
        }
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _productService.DeleteById(id);
            return Ok();
        }
    }
}
