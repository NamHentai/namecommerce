﻿using Microsoft.AspNetCore.Mvc;
using NamECommerce.Entities;
using NamECommerce.Entities.Dtos;
using NamECommerce.Services.IServices;

namespace NamECommerce.Controllers
{
    [Route("api/orders")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var serviceResponse = await _orderService.GetAll();
            return Ok(serviceResponse._result);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var serviceResponse = await _orderService.GetById(id);
            return Ok(serviceResponse._result);
        }
        [HttpPost]
        public async Task<IActionResult> Post(OrderCreateDto orderCreateDto)
        {
            var serviceResponse = await _orderService.Create(orderCreateDto);
            return Ok(serviceResponse._result);
        }
        [HttpPut]
        public async Task<IActionResult> Put(OrderDto orderDto)
        {
            return Ok();
        }
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _orderService.DeleteById(id);
            return Ok();
        }
        [HttpGet("{id}/orderItems")]
        public async Task<IActionResult> GetOrderItems(Guid id)
        {
            var serviceResponse = await _orderService.GetById(id);

            return Ok(serviceResponse._result);
        }
        [HttpPost("{id}/orderItems")]
        public async Task<IActionResult> PostOrderItems(Guid id, [FromQuery] OrderItemCreateDto orderItemCreateDto)
        {
            if (await _orderService.GetById(id) != null)
            {
                return BadRequest();
            }
            OrderItemDto orderItemDto = new OrderItemDto();
            orderItemDto.OrderId = id;
            orderItemDto.ProductId = orderItemCreateDto.ProductId;
            orderItemDto.Quantity = orderItemCreateDto.Quantity;
            var result = await _orderService.AddOrderItem(orderItemDto);
            return Ok();
        }
        [HttpPut("{id}/orderItems")]
        public async Task<IActionResult> PutOrderItems(Guid id, [FromQuery] OrderItemCreateDto orderItemCreateDto)
        {
            if (await _orderService.GetById(id) != null)
            {
                return BadRequest();
            }
            OrderItemDto orderItemDto = new OrderItemDto();
            orderItemDto.OrderId = id;
            orderItemDto.ProductId = orderItemCreateDto.ProductId;
            orderItemDto.Quantity = orderItemCreateDto.Quantity;
            var result = await _orderService.UpdateOrderItem(orderItemDto);
            return Ok();
        }
        [HttpDelete("{id}/orderItems")]
        public async Task<IActionResult> DeleteOrderItems(Guid orderId, Guid orderItemId)
        {
            if (await _orderService.GetById(orderId) != null)
            {
                return BadRequest();
            }
            var result = await _orderService.DeleteById(orderId);
            return Ok();
        }
        [HttpGet("search")]
        public async Task<IActionResult> GetBySearch([FromQuery] string customerId = null,
            string CustomerId = null)
        {
            var orders = await _orderService.GetAllAdvanceAsync(x => x.CustomerId.ToString().Contains(customerId));
            return Ok(orders._result);
        }
    }
}
