﻿using Microsoft.AspNetCore.Mvc;
using NamECommerce.Entities.Dtos;
using NamECommerce.Services.IServices;

namespace NamECommerce.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IOrderService _orderService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CustomerCreateDto customer)
        {
            await _customerService.Create(customer);
            return Ok();
        }
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var customers = await _customerService.GetAll();
            return Ok(customers._result);
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var customer = await _customerService.GetById(id);
            return Ok(customer);
        }
        [HttpGet("{id}/orders")]
        public async Task<IActionResult> GetOrders(Guid id)
        {
            var orders = await _customerService.GetOrders(id);
            return Ok(orders);
        }
        [HttpPost("{id}/orders")]
        public async Task<IActionResult> PostOrder(Guid id, [FromQuery] OrderDto orderDto)
        {
            if (await _customerService.GetById(id) != null)
            {
                return BadRequest();
            }
            OrderCreateDto orderCreateDto = new OrderCreateDto();
            orderCreateDto.CustomerId = id;
            orderCreateDto.OrderItems = orderDto.OrderItems;
            var result = await _orderService.Create(orderCreateDto);
            return Ok();
        }
        [HttpPut("{id}/orders")]
        public async Task<IActionResult> PutOrder(Guid id, [FromQuery] OrderDto orderDto)
        {
            if (await _customerService.GetById(id) != null)
            {
                return BadRequest();
            }
            OrderCreateDto orderCreateDto = new OrderCreateDto();
            orderCreateDto.CustomerId = id;
            orderCreateDto.OrderItems = orderDto.OrderItems;
            var result = await _orderService.Create(orderCreateDto);
            return Ok();
        }
        [HttpDelete("{id}/orders")]
        public async Task<IActionResult> DeleteOrder(Guid customerid, Guid orderId)
        {
            if (await _customerService.GetById(customerid) != null)
            {
                return BadRequest();
            }
            var result = await _orderService.DeleteById(orderId);
            return Ok();
        }
        [HttpGet("search")]
        public async Task<IActionResult> GetBySearch([FromQuery] string name = null,
            string email = null,
            string address = null,
            string phone = null)
        {
            var customer = await _customerService.GetAllAdvanceAsync(x => x.Name.Contains(name) ||
             x.Address.Contains(address) || x.Phone.Contains(phone) || x.Email.Contains(email));
            return Ok(customer._result);
        }
        [HttpPut]
        public async Task<IActionResult> Put(CustomerDto customerDto)
        {
            var customer = await _customerService.Update(customerDto);
            return Ok(customer);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> SoftDelete(Guid id)
        {
            await _customerService.SoftDeleteById(id);
            return Ok();
        }
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _customerService.DeleteById(id);
            return Ok();
        }
    }
}